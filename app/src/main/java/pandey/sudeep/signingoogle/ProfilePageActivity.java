package pandey.sudeep.signingoogle;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class ProfilePageActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);

        //Configure sign-in to request the user's ID, email address, and basic
        //profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        final Intent intent = getIntent();

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(intent.getExtras().getString("name"));

        TextView following = (TextView)findViewById(R.id.following);
        TextView followingVal = (TextView)findViewById(R.id.following_val);
        TextView followers = (TextView)findViewById(R.id.followers);
        TextView followersVal = (TextView)findViewById(R.id.followers_val);

        TextView details = (TextView)findViewById(R.id.details);
        UIUtils.autoSizeText(details);

        UIUtils.autoSizeText(name);
        UIUtils.autoSizeText(following);
        UIUtils.autoSizeText(followingVal);
        UIUtils.autoSizeText(followers);
        UIUtils.autoSizeText(followersVal);

        final AppExecutors executors = new AppExecutors();
        executors.networkIO().execute(new Runnable() {
            Bitmap bitmap;
            @Override
            public void run() {
                bitmap = UIUtils.getImageBitmap(intent.getExtras().getString("photoURI"));
                //bitmap = UIUtils.getImageBitmap("https://lh3.googleusercontent.com/-yKIpZrftHKw/AAAAAAAAAAI/AAAAAAAAAKs/XcRS5wp74U8/photo.jpg");
                executors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),bitmap);
                        roundedBitmapDrawable.setCircular(true);
                        ((ImageView)findViewById(R.id.profilePic)).setImageDrawable(roundedBitmapDrawable);
                        //((ImageView)findViewById(R.id.profilePic)).setImageBitmap(bitmap);
                    }
                });
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_action_name);
        toolbar.setTitle("");

        setSupportActionBar(toolbar);

        collapsingToolbarLayout = findViewById(R.id.collapse);
        collapsingToolbarLayout.setTitle(intent.getExtras().getString("name"));
        collapsingToolbarLayout.setTitleEnabled(false);

        final BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        // implementation
                        return true;

                    case R.id.fragment:
                        //implementation
                        return true;

                    case R.id.service:
                        //implementation
                        return true;

                    case R.id.alarm:
                        //implementation
                        return true;

                    case R.id.other:
                        //implementation
                        return true;
                    default:
                }
                return false;
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                // implementation
                signOut();
                return true;
            default:
                // implementation
                return super.onOptionsItemSelected(item);

        }
    }

    private void signOut(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                });
    }

}
