package pandey.sudeep.signingoogle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private GoogleSignInClient mGoogleSignInClient;
    private static int RC_SIGN_IN = 1;
    private static final String TAG = MainActivity.class.getName();

    private GoogleSignInOptions gso;

    private HorizontalScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = (HorizontalScrollView)findViewById(R.id.horizontalScrollView);

        TextView titleDisplay = (TextView)findViewById(R.id.title);
        titleDisplay.setText(BuildConfig.MainTitle);

        List<TextView> textViewContainer = new ArrayList<TextView>();
        textViewContainer.add(titleDisplay);
        textViewContainer.add((TextView) findViewById(R.id.version));
        textViewContainer.add((TextView) findViewById(R.id.fragment));
        textViewContainer.add((TextView) findViewById(R.id.service));
        textViewContainer.add((TextView) findViewById(R.id.broadcast));
        textViewContainer.add((TextView) findViewById(R.id.jobscheduler));
        textViewContainer.add((TextView) findViewById(R.id.multithreading));
        textViewContainer.add((TextView) findViewById(R.id.loader));
        textViewContainer.add((TextView) findViewById(R.id.recyclerView));
        textViewContainer.add((TextView) findViewById(R.id.persistence));
        textViewContainer.add((TextView) findViewById(R.id.provider));
        textViewContainer.add((TextView) findViewById(R.id.alarms));
        textViewContainer.add((TextView) findViewById(R.id.material));

        for (int i=0;i<textViewContainer.size();i++){
            UIUtils.autoSizeText(textViewContainer.get(i));
        }

        //Configure sign-in to request the user's ID, email address, and basic
        //profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        findViewById(R.id.sign_in_button).setOnClickListener(this);
    }

    @Override
    protected void onStart(){
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{ scrollView.getScrollX(), scrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
        if(position != null)
            scrollView.post(new Runnable() {
                public void run() {
                    scrollView.scrollTo(position[0], position[1]);
                }
            });
    }

    private void updateUI(GoogleSignInAccount acct){

        Intent intent = new Intent(this,ProfilePageActivity.class);
        if (acct != null) {
            intent.putExtra("name",acct.getDisplayName());
            //intent.putExtra("familyName",acct.getFamilyName());
            //intent.putExtra("givenName",acct.getGivenName());
            //intent.putExtra("email",acct.getEmail());
            //intent.putExtra("id",acct.getId());
            //intent.putExtra("token",acct.getIdToken());
            //intent.putExtra("photoURI",acct.getPhotoUrl()!=null?acct.getPhotoUrl().toString():null);
            intent.setData(acct.getPhotoUrl());
            startActivity(intent);
        }
    }

    public void onClick(View view){
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }
}
