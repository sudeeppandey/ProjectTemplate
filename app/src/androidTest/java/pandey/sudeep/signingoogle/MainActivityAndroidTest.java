package pandey.sudeep.signingoogle;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.widget.EditText;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityAndroidTest {

    private UiDevice mUiDevice;
    @Rule
    public ActivityTestRule<MainActivity> mNotesActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void before() throws Exception {
        mUiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @Test
    public void testUIComponentsDisplayAppropriately() throws UiObjectNotFoundException {
        onView(withId(R.id.sign_in_button)).perform(click());

        UiObject mText = mUiDevice.findObject(new UiSelector().textContains("Sudeep"));
        mText.click();

        //onView(withText("Add account")).perform(click());

        //UiObject mText2 = mUiDevice.findObject(new UiSelector().text("Email or phone"));
        //mText2.setText("exec_rep@pandtek.com");

        //UiObject nextButton = mUiDevice.findObject(new UiSelector().description("NEXT"));
        //nextButton.click();

        //UiObject mText3 = mUiDevice.findObject(new UiSelector().text("Enter your password"));
        //mText3.setText("alterbridge1");

        //UiObject nextButton2 = mUiDevice.findObject(new UiSelector().description("NEXT"));
        //nextButton2.click();

        //UiObject skipButton = mUiDevice.findObject(new UiSelector().instance(3));
        //UiObject skipButton = mUiDevice.findObject(new UiSelector().description("I AGREE"));
        //skipButton.click();
    }
}