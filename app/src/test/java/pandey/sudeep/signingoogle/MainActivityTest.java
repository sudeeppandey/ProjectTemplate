package pandey.sudeep.signingoogle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import static android.support.v4.widget.TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Matchers.any;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowTypeface;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@PowerMockIgnore({ "org.mockito.*", "org.robolectric.*", "android.*" })
public class MainActivityTest {

    @Mock
    GoogleSignInClient googleSignInClient;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Test
    public void testUIComponentsDisplayAppropriately(){

       Activity activity = Robolectric.buildActivity(MainActivity.class).create().get();

       TextView title = (TextView)activity.findViewById(R.id.title);
       TextView version = (TextView)activity.findViewById(R.id.version);
       HorizontalScrollView scrollView = (HorizontalScrollView) activity.findViewById(R.id.horizontalScrollView);
       //SignInButton button = (SignInButton)activity.findViewById(R.id.sign_in_button);
       activity.findViewById(R.id.sign_in_button);

       assertNotNull(title);
       assertNotNull(version);
       assertNotNull(scrollView);
       assertNotNull(activity.findViewById(R.id.sign_in_button));

       assertEquals(BuildConfig.MainTitle,title.getText());
       assertEquals(activity.getResources().getString(R.string.version_Info),version.getText());

       ShadowTypeface shadowTypeface = shadowOf(title.getTypeface());

       assertEquals(1,shadowTypeface.getFontDescription().style);
       //assertEquals("abril_fatface",shadowTypeface.getFontDescription().getFamilyName());
    }

    @Test @Config(qualifiers = "+port")
    public void testDeviceConfigurationChange(){

        ActivityController controller = Robolectric.buildActivity(MainActivity.class);
        //controller.setup();

        Activity activity = (Activity)controller.create().start().resume().visible().get();
        HorizontalScrollView scrollView = activity.findViewById(R.id.horizontalScrollView);
        scrollView.scrollTo(5,0);

        assertEquals(Configuration.ORIENTATION_PORTRAIT,activity.getResources().getConfiguration().orientation);
        assertEquals(0,scrollView.getScrollY());
        assertEquals(5,scrollView.getScrollX());

        RuntimeEnvironment.setQualifiers("+land");
        controller.configurationChange();

        assertEquals(activity.getResources().getConfiguration().orientation,Configuration.ORIENTATION_LANDSCAPE);
        assertEquals(5,scrollView.getScrollX());
        assertEquals(0,scrollView.getScrollY());
    }

    @Test
    public void testAutoSizeTextTypeIsValid(){

        Activity activity = Robolectric.buildActivity(MainActivity.class).create().get();

        TextView titleDisplay = (TextView)activity.findViewById(R.id.title);

        List<TextView> textViewContainer = new ArrayList<TextView>();
        textViewContainer.add(titleDisplay);
        textViewContainer.add((TextView)activity.findViewById(R.id.version));
        textViewContainer.add((TextView)activity.findViewById(R.id.fragment));
        textViewContainer.add((TextView)activity.findViewById(R.id.service));
        textViewContainer.add((TextView)activity.findViewById(R.id.broadcast));
        textViewContainer.add((TextView)activity.findViewById(R.id.jobscheduler));
        textViewContainer.add((TextView)activity.findViewById(R.id.multithreading));
        textViewContainer.add((TextView)activity.findViewById(R.id.loader));
        textViewContainer.add((TextView)activity.findViewById(R.id.recyclerView));
        textViewContainer.add((TextView)activity.findViewById(R.id.persistence));
        textViewContainer.add((TextView)activity.findViewById(R.id.provider));
        textViewContainer.add((TextView)activity.findViewById(R.id.alarms));
        textViewContainer.add((TextView)activity.findViewById(R.id.material));

        for (int i=0;i<textViewContainer.size();i++){
            assertEquals(AUTO_SIZE_TEXT_TYPE_UNIFORM,textViewContainer.get(i).getAutoSizeTextType());
        }
    }

    @Test
    public void getSignInIntentReturnsNonNull(){

        Activity activity = Robolectric.buildActivity(MainActivity.class).create().get();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        Intent signInIntent = GoogleSignIn.getClient(activity,gso).getSignInIntent();
        assertNotNull(signInIntent);
        assertNotNull(signInIntent.getComponent());
        assertEquals(signInIntent.getComponent().getClassName(),"com.google.android.gms.auth.api.signin.internal.SignInHubActivity");
        //com.google.android.gms.auth.api.signin.internal.SignInHubActivity
    }

    @Test
    @PrepareForTest(GoogleSignIn.class)
    public void testOnStart(){

        PowerMockito.mockStatic(GoogleSignIn.class);
        GoogleSignInAccount googleSignInAccount = PowerMockito.mock(GoogleSignInAccount.class);
        when(GoogleSignIn.getLastSignedInAccount(any(Context.class))).thenReturn(googleSignInAccount);

        Activity activity = Robolectric.buildActivity(MainActivity.class).create().start().get();

        Intent expectedIntent = new Intent(activity,ProfilePageActivity.class);
        Intent actualIntent = ShadowApplication.getInstance().getNextStartedActivity();

        assertEquals(expectedIntent.getComponent(),actualIntent.getComponent());
    }
}
